export class LibraryModel {
  readonly anime: string;

  readonly status: number;

  readonly episodes: number;

  readonly createdAt: string;

  readonly updatedAt: string;

  constructor(
    anime: string,
    status: number,
    episodes: number,
    createdAt: string,
    updatedAt: string,
  ) {
    this.anime = anime;
    this.status = status;
    this.episodes = episodes;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
