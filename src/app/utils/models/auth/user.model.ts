export class UserModel {
  public email: any;

  public photo: string;

  public cover: string;

  public tachi: string;

  public stats: any;

  public membership: any;

  public rank: any;

  public online: any;

  public notifications: any;

  public name: string;

  public username: string;

  public phrase: string;

  public sessions: any;

  public library: any;
}
