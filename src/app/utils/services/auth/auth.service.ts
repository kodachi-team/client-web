import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../../environments/environment';
import { SignInI } from '../../interfaces/auth/signin.interface';
import { SignUpI } from '../../interfaces/auth/signup.interface';
import { SignOutI } from '../../interfaces/auth/signout.interface';
import { PasswordRequestI } from '../../interfaces/auth/password.request.interface';
import { ChangePasswordI } from '../../interfaces/auth/password.change.interface';
import { ActivateAccountI } from '../../interfaces/auth/account.activate.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  public signInAccount(signInData: SignInI) {
    return this.httpClient.post(`${environment.api}/auth/signin`, signInData);
  }

  public signUpAccount(signUpData: SignUpI) {
    return this.httpClient.put(`${environment.api}/auth/signup`, signUpData);
  }

  public getUser(userId: string) {
    return this.httpClient.get(`${environment.api}/user/get/${userId}`);
  }

  public signOutAccount(signOutData: SignOutI) {
    return this.httpClient.post(
      `${environment.api}/auth/account/logout`,
      signOutData,
    );
  }

  public requestPasswordChange(requestPasswordData: PasswordRequestI) {
    return this.httpClient.post(
      `${environment.api}/auth/account/password/reset`,
      requestPasswordData,
    );
  }

  public changePasswordAccount(changePasswordData: ChangePasswordI) {
    return this.httpClient.patch(
      `${environment.api}/auth/account/password/change`,
      changePasswordData,
    );
  }

  public activateAccount(activateAccountData: ActivateAccountI) {
    return this.httpClient.patch(
      `${environment.api}/auth/account/verify/email`,
      activateAccountData,
    );
  }

  public getToken() {
    if (localStorage.getItem('token') !== null) {
      return localStorage.getItem('token');
    } else {
      return 'Waiting...';
    }
  }
}
