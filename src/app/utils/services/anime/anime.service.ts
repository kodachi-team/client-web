import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AnimeService {
  constructor(private httpClient: HttpClient) {}

  public getNewAnime(page: number, limit: number) {
    return this.httpClient.get(
      `${environment.api}/anime/get/new?limit=${limit}&page=${page}`,
    );
  }

  public getRecomendationAnime(page: number, limit: number, userId: string) {
    return this.httpClient.post(`${environment.api}/anime/get/recomendations`, {
      userId,
      page,
      limit,
    });
  }

  public getLastSeasonAnime(
    page: number,
    limit: number,
    startDate: string,
    endDate: string,
  ) {
    return this.httpClient.get(
      `${environment.api}/anime/get/season?startDate=${startDate}&endDate=${endDate}&sort=-release.start&limit=${limit}&page=${page}`,
    );
  }

  public getRandomAnime(limit: number) {
    return this.httpClient.get(
      `${environment.api}/anime/get/random?&limit=${limit}`,
    );
  }

  public getRatedSeasonAnime(
    page: number,
    limit: number,
    startDate: string,
    endDate: string,
  ) {
    return this.httpClient.get(
      `${environment.api}/anime/get/season?startDate=${startDate}&endDate=${endDate}&sort=-rate&limit=${limit}&page=${page}`,
    );
  }

  public getRatedAnimeAllTime(page: number, limit: number) {
    return this.httpClient.get(
      `${environment.api}/anime/get/best/all-time?limit=${limit}&page=${page}`,
    );
  }

  public getRecentAdded(page: number, limit: number) {
    return this.httpClient.get(
      `${environment.api}/anime/get/recent?limit=${limit}&page=${page}`,
    );
  }

  public getByCategory(page: number, limit: number, category: number) {
    return this.httpClient.get(
      `${environment.api}/anime/get/animes?category=${category}&limit=${limit}&page=${page}&sort=-title.romanized`,
    );
  }

  public getCategoryAnimeList(): any[] {
    return [
      {
        id: 1,
        name: 'Comedy',
      },
      {
        id: 2,
        name: 'Fantasy',
      },
      {
        id: 3,
        name: 'Romance',
      },
      {
        id: 4,
        name: 'Action',
      },
      {
        id: 5,
        name: 'School life',
      },
      {
        id: 6,
        name: 'Drama',
      },
      {
        id: 7,
        name: 'Adventure',
      },
      {
        id: 8,
        name: 'Slice of Life',
      },
      {
        id: 9,
        name: 'Shoujo Ai',
      },
      {
        id: 10,
        name: 'Science Fiction',
      },
      {
        id: 11,
        name: 'Yaoi',
      },
      {
        id: 12,
        name: 'Ecchi',
      },
      {
        id: 13,
        name: 'Sports',
      },
      {
        id: 14,
        name: 'Japan',
      },
      {
        id: 15,
        name: 'Historical',
      },
      {
        id: 16,
        name: 'Earth',
      },
      {
        id: 17,
        name: 'Thriller',
      },
      {
        id: 18,
        name: 'Harem',
      },
      {
        id: 19,
        name: 'Mystery',
      },
      {
        id: 20,
        name: 'Present',
      },
      {
        id: 21,
        name: 'Magic',
      },
      {
        id: 22,
        name: 'Asia',
      },
      {
        id: 23,
        name: 'Kids',
      },
      {
        id: 24,
        name: 'Horror',
      },
      {
        id: 25,
        name: 'Music',
      },
      {
        id: 26,
        name: 'Mecha',
      },
      {
        id: 27,
        name: 'Psychological',
      },
      {
        id: 28,
        name: 'Super Power',
      },
      {
        id: 29,
        name: 'Shounen Ai',
      },
      {
        id: 30,
        name: 'Martial Arts',
      },
      {
        id: 31,
        name: 'Demon',
      },
      {
        id: 32,
        name: 'Military',
      },
      {
        id: 33,
        name: 'Plot Continuity',
      },
      {
        id: 34,
        name: 'Fantasy World',
      },
      {
        id: 35,
        name: 'Violence',
      },
      {
        id: 36,
        name: 'Shounen',
      },
      {
        id: 37,
        name: 'Motorsport',
      },
      {
        id: 38,
        name: 'Parody',
      },
      {
        id: 39,
        name: 'Isekai',
      },
      {
        id: 40,
        name: 'Seinen',
      },
      {
        id: 41,
        name: 'Contemporary Fantasy',
      },
      {
        id: 42,
        name: 'Sudden Girlfriend Appearance',
      },
      {
        id: 43,
        name: 'Friendship',
      },
      {
        id: 44,
        name: 'Parental Abandonment',
      },
      {
        id: 45,
        name: 'The Arts',
      },
      {
        id: 46,
        name: 'Angst',
      },
      {
        id: 47,
        name: 'Past',
      },
    ];
  }
}
