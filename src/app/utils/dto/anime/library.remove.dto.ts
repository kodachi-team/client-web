export class LibraryAnimeRemoveDto {
  readonly userId: string;

  readonly animeId: string;
}
