export class LibraryAnimeAddDto {
  readonly userId: string;

  readonly animeId: string;

  readonly status: number;

  readonly episodes: number;
}
