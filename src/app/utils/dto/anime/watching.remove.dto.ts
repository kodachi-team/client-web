export class WatchingAnimeRemoveDto {
  readonly userId: string;

  readonly animeId: string;
}
