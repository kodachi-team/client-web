export class ReviewAnimeAddDto {
  readonly animeId: string;

  readonly author: string;

  readonly rate: number;

  readonly spoiler: boolean;

  readonly content: string;

  readonly ip: string;

  readonly device: string;

  readonly session: string;
}
