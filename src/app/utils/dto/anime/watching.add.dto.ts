export class WatchingAnimeAddDto {
  readonly animeId: string;

  readonly authorId: string;

  readonly status: number;

  readonly finished: boolean;
}
