export class FollowerAnimeRemoveDto {
  readonly animeId: string;

  readonly userId: string;
}
