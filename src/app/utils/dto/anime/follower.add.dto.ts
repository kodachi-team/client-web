export class FollowerAnimeAddDto {
  readonly animeId: string;

  readonly author: string;

  readonly ip: string;

  readonly device: string | string;

  readonly session: string;
}
