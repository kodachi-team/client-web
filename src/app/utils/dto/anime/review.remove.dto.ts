export class ReviewAnimeRemoveDto {
  readonly animeId: string;

  readonly userId: string;
}
