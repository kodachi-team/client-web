export class LibraryAnimeUpdateDto {
  readonly animeId: string;

  readonly userId: string;

  readonly status: number;
}
