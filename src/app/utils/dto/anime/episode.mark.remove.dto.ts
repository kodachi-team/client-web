export class EpisodeViewAnimeRemoveDto {
  readonly animeId: string;

  readonly userId: string;

  readonly episodeId: string;
}
