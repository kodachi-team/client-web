export class EpisodeViewAnimeAddDto {
  readonly animeId: string;

  readonly userId: string;

  readonly episodeId: string;
}
