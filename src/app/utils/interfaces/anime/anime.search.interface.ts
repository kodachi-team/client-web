export interface SearchAnimesI {
  readonly sort: string;

  readonly limit: number;

  readonly page: number;

  readonly title: string;

  readonly season: number;

  readonly categories: string;

  readonly status: number;

  readonly type: number;

  readonly nsfw: number;

  readonly release: string;

  readonly finish: string;
}
