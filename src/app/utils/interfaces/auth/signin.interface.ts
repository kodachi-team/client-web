export interface SignInI {
  readonly email: string;

  readonly password: string;

  readonly device: string;

  readonly ip: string;
}
