export interface SignOutI {
  readonly userId: string;

  readonly sesionId: string;
}
