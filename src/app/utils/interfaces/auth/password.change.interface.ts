export interface ChangePasswordI {
  readonly userEmail: string;

  readonly tokenPassword: string;

  readonly newPassword: string;
}
