export interface ActivateAccountI {
  readonly email: string;

  readonly token: string;
}
