export class SignUpKey {
  readonly key: string;
}

export class SignUpI {
  readonly email: SignUpKey;

  readonly name: string;

  readonly username: string;

  readonly categories: string[];

  readonly ip: string;

  readonly device: string | number;

  readonly password: SignUpKey;
}
