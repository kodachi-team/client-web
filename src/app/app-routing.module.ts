import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthModule),
    data: { showMainMenu: false },
  },
  {
    path: 'explorer',
    loadChildren: () =>
      import('./pages/explorer/explorer.module').then((m) => m.ExplorerModule),
    data: { showMainMenu: true },
  },
  {
    path: '',
    redirectTo: 'explorer',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
