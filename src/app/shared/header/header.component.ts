import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from '../../app-reducer.store';
import { initSignOutAction, authActionsEnum } from '../../store';

import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public openMenuProfile: boolean = false;
  public isAuth: boolean;
  public isShowMenu: boolean = false;
  public isLoadigStoreAuth: boolean;
  public styleButtonMenu: string = 'default';

  public userPhoto: string = '../../../assets/sidebar/photo-default.jpg';
  public userCover: string = '../../../assets/sidebar/bg-default.jpg';
  public userName: string = 'Kodachi';
  public userPhrase: string = 'The best anime tracker';
  public userAnimeLibrary: number = 0;
  public userAnimeWatching: number = 0;
  public userAnimeViews: number = 0;
  public userAnimeDropped: number = 0;
  public userRanks: any[] = [];

  private authStoreSubscription: Subscription = new Subscription();
  private authLogOutStoreSubscription: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private action: Actions,
    private notification: NzNotificationService,
  ) {
    this.authLogOutStoreSubscription = this.action
      .pipe(ofType(authActionsEnum.LOG_OUT_ACCOUNT))
      .subscribe(() => {
        setTimeout(() => {
          localStorage.removeItem('id');
          localStorage.removeItem('token');
          localStorage.removeItem('session');

          this.isAuth = false;
          this.userPhoto = '../../../assets/sidebar/photo-default.jpg';
          this.userCover = '../../../assets/sidebar/bg-default.jpg';
          this.userName = 'Kodachi';
          this.userPhrase = 'The best anime tracker';
          this.userAnimeLibrary = 0;
          this.userAnimeWatching = 0;
          this.userAnimeViews = 0;
          this.userAnimeDropped = 0;
          this.userRanks = [];

          this.notification.create(
            'success',
            'We wait for you soon (︶︹︺)',
            'At some point, you will log in again, I hope sos.',
          );
        }, 1500);
      });
  }

  ngOnInit(): void {
    this.authStoreSubscription = this.store
      .select('auth')
      .pipe(filter((data) => data.user != null))
      .subscribe((data) => {
        this.isAuth = data.session;
        this.userPhoto = data.user.photo;
        this.userCover = data.user.cover;
        this.userName = data.user.name;
        this.userPhrase = data.user.phrase;
        this.userAnimeLibrary = data.user.library.length;
        this.userAnimeWatching = data.user.stats.watching;
        this.userAnimeViews = data.user.stats.views;
        this.userAnimeDropped = data.user.stats.dropped;

        if (data.user.rank.admin) {
          this.userRanks.push({ title: 'Administrator', color: '#ff4d4f' });
        }

        if (data.user.rank.mod) {
          this.userRanks.push({ title: 'Moderator', color: '#fadb14' });
        }

        if (data.user.membership.status) {
          this.userRanks.push({ title: 'Premium', color: '#faad14' });
        }
      });
  }

  ngOnDestroy(): void {
    this.authStoreSubscription.unsubscribe();
    this.authLogOutStoreSubscription.unsubscribe();
  }

  public signOut() {
    const isValidId = new RegExp('^[0-9a-fA-F]{24}$');
    if (
      isValidId.test(localStorage.getItem('id')) &&
      isValidId.test(localStorage.getItem('session'))
    ) {
      this.store.dispatch(
        initSignOutAction({
          userId: localStorage.getItem('id'),
          sessionId: localStorage.getItem('session'),
        }),
      );
    }
  }

  public showMenu() {
    this.isShowMenu = !this.isShowMenu;
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (window.scrollY == 0) {
      this.styleButtonMenu = 'default';
    } else {
      this.styleButtonMenu = 'primary';
    }
  }
}
