import { ActionReducerMap } from '@ngrx/store';
import {
  settingsReducer,
  settingsStateI,
  authStateI,
  authReducer,
  animeStateI,
  animeReducer,
} from './store';

export interface AppState {
  settings: settingsStateI;
  auth: authStateI;
  anime: animeStateI;
}

export const appReducers: ActionReducerMap<AppState> = {
  settings: settingsReducer,
  auth: authReducer,
  anime: animeReducer,
};
