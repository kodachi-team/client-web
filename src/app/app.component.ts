import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { Store } from '@ngrx/store';
import { setVisibilityHeaderFooter } from './store/settings/settings.action';
import { AppState } from './app-reducer.store';

import { Subscription, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { initChangeAuthState } from './store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit, OnDestroy {
  public routerSubscribe: Subscription = new Subscription();
  public settingsStoreSubscribe: Subscription = new Subscription();

  public showMenu: boolean;
  public bgColorHeader: string = 'rgba(25, 25, 25, 0.2)';

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private activateRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    const isValidId = new RegExp('^[0-9a-fA-F]{24}$');
    if (isValidId.test(localStorage.getItem('id'))) {
      this.store.dispatch(
        initChangeAuthState({ userId: localStorage.getItem('id') }),
      );
    }

    this.routerSubscribe = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe({
        next: () => {
          this.store.dispatch(
            setVisibilityHeaderFooter({
              visibility: this.activateRoute.root.firstChild.snapshot.data[
                'showMainMenu'
              ],
            }),
          );
        },
        error: () => {
          this.router.navigate(['/app-error']);
        },
      });

    this.settingsStoreSubscribe = this.store
      .select('settings')
      .subscribe((data) => {
        this.showMenu = data.showHeaderFooter;
      });
  }

  ngOnDestroy(): void {
    this.routerSubscribe.unsubscribe();
    this.settingsStoreSubscribe.unsubscribe();
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (window.scrollY == 0) {
      this.bgColorHeader = 'rgba(25, 25, 25, 0.1)';
    } else {
      this.bgColorHeader = '#191919';
    }
  }
}
