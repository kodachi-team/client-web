import { Component, OnInit, Input } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 })),
      ]),
      transition(':leave', [animate(500, style({ opacity: 0 }))]),
    ]),
  ],
})
export class CategoryComponent implements OnInit {
  @Input() animesData: any[] = [];
  @Input() title: string;
  @Input() loading: boolean;
  @Input() error: boolean;
  @Input() margin_top: boolean;

  public loadingViewsTemplate: any[] = [1, 2, 3, 4, 5, 6];

  constructor() {}

  ngOnInit(): void {}
}
