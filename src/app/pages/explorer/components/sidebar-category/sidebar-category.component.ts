import { Component, OnInit } from '@angular/core';

import { AnimeService } from '../../../../utils/services/anime/anime.service';

@Component({
  selector: 'app-sidebar-category',
  templateUrl: './sidebar-category.component.html',
  styleUrls: ['./sidebar-category.component.less'],
})
export class SidebarCategoryComponent implements OnInit {
  public categoryList: any[] = [];
  openMap: { [name: string]: boolean } = {
    sub1: true,
    sub2: false,
    sub3: false,
  };

  constructor(private animeService: AnimeService) {}

  ngOnInit(): void {
    this.categoryList = this.animeService.getCategoryAnimeList();
  }

  public openHandler(value: string): void {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }
}
