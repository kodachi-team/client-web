import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../app-reducer.store';
import { authActionsEnum, initLoadRecomendationAnime } from '../../../../store';
import { Actions, ofType } from '@ngrx/effects';

import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.less'],
})
export class CarouselComponent implements OnInit, OnDestroy {
  public animeRated: any[] = [
    {
      title: {
        romanized: 'Loading anime...',
      },
      cover: '../../../../../assets/status/no_cover.jpg',
      synopsis: 'Wait while we get the best anime for you.',
      rate: 0,
      favorites_count: 0,
    },
  ];
  public animeRecomendationSubscription: Subscription = new Subscription();
  public authStateLogOutSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();
  public statusBtnViewAnime: string = "You can't touch here yet";

  constructor(private store: Store<AppState>, private $actions: Actions) {
    this.authStateLogOutSubscription = this.$actions
      .pipe(ofType(authActionsEnum.LOG_OUT_ACCOUNT))
      .subscribe(() => {
        setTimeout(() => {
          this.animeRecomendationSubscription = this.store
            .select('anime')
            .pipe(filter((data) => data.recomendation_anime.data != null))
            .subscribe((data) => {
              this.statusBtnViewAnime = "Let's see what you think";
              this.animeRated = data.rated_anime.data;
            });
        }, 1500);
      });
  }

  ngOnInit(): void {
    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      if (data.session) {
        this.store.dispatch(
          initLoadRecomendationAnime({
            page: 0,
            limit: 24,
            userId: localStorage.getItem('id'),
          }),
        );

        this.animeRecomendationSubscription = this.store
          .select('anime')
          .pipe(filter((data) => data.recomendation_anime.data != null))
          .subscribe((data) => {
            this.statusBtnViewAnime = "Let's see what you think";
            this.animeRated = data.recomendation_anime.data;
          });
      } else {
        this.animeRecomendationSubscription = this.store
          .select('anime')
          .pipe(filter((data) => data.rated_anime.data != null))
          .subscribe((data) => {
            this.statusBtnViewAnime = "Let's see what you think";
            this.animeRated = data.rated_anime.data;
          });
      }
    });
  }

  ngOnDestroy(): void {
    this.animeRecomendationSubscription.unsubscribe();
    this.authStateLogOutSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();
  }
}
