import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadAnimeCategoryAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { environment } from '../../../../../../../environments/environment';
import { AnimeService } from '../../../../../../utils/services/anime/anime.service';

@Component({
  templateUrl: './by-category.component.html',
  styleUrls: ['./by-category.component.less'],
})
export class ByCategoryComponent implements OnInit, OnDestroy {
  public animeByCategory: any[] = [];
  public isLoadingAnimeByCategory: boolean = true;
  public isErrorAnimeByCategory: boolean = false;
  public categoryName: string = 'Unknown category';

  public isAuth: boolean;
  private categoryId: number;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();
  private routerSubscription: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private animeService: AnimeService,
    private title: Title,
  ) {}

  ngOnInit(): void {
    this.routerSubscription = this.activatedRoute.params.subscribe((params) => {
      this.categoryId = params['category'];
      this.convertCategory(params['category']);
      this.title.setTitle(
        `${this.convertCategory(params['category'])} - Explorer ${
          environment.title
        }`,
      );
      this.store.dispatch(
        initLoadAnimeCategoryAnime({
          page: 0,
          limit: 36,
          category: params['category'],
        }),
      );
    });

    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.category_anime.data != null))
      .subscribe((data) => {
        this.animeByCategory = data.category_anime.data;
        this.isLoadingAnimeByCategory = data.category_anime.loading;
        this.isErrorAnimeByCategory = data.category_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();
    this.routerSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateByCategoryAnimeList(page: number) {
    this.store.dispatch(
      initLoadAnimeCategoryAnime({
        page,
        limit: 36,
        category: this.categoryId,
      }),
    );
  }

  public convertCategory(categoryId: number) {
    this.animeService.getCategoryAnimeList().forEach((category: any) => {
      if (category.id == categoryId) {
        this.categoryName = category.name;
      }
    });
  }
}
