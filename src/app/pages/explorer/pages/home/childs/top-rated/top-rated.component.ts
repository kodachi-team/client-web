import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadSeasonRatedAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './top-rated.component.html',
  styleUrls: ['./top-rated.component.less'],
})
export class TopRatedComponent implements OnInit, OnDestroy {
  public animeTopRatedSeason: any[] = [];
  public isLoadingAnimeTopRatedSeason: boolean = true;
  public isErrorAnimeTopRatedSeason: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(
      `Top rated this season - Explorer ${environment.title}`,
    );
    this.store.dispatch(
      initLoadSeasonRatedAnime({
        page: 0,
        limit: 24,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.rated_season_anime.data != null))
      .subscribe((data) => {
        this.animeTopRatedSeason = data.rated_season_anime.data;
        this.isLoadingAnimeTopRatedSeason = data.rated_season_anime.loading;
        this.isErrorAnimeTopRatedSeason = data.rated_season_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateTopRatedSeasonAnimeList(page: number) {
    this.store.dispatch(
      initLoadSeasonRatedAnime({
        page,
        limit: 24,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
  }
}
