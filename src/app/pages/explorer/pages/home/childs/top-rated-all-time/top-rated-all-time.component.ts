import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadRatedAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './top-rated-all-time.component.html',
  styleUrls: ['./top-rated-all-time.component.less'],
})
export class TopRatedAllTimeComponent implements OnInit, OnDestroy {
  public animeTopRatedAllTime: any[] = [];
  public isLoadingAnimeTopRatedAllTime: boolean = true;
  public isErrorAnimeTopRatedAllTime: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(`Top rated all time - Explorer ${environment.title}`);
    this.store.dispatch(initLoadRatedAnime({ page: 0, limit: 36 }));
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.rated_anime.data != null))
      .subscribe((data) => {
        this.animeTopRatedAllTime = data.rated_anime.data;
        this.isLoadingAnimeTopRatedAllTime = data.rated_anime.loading;
        this.isErrorAnimeTopRatedAllTime = data.rated_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateTopRatedAllTimeAnimeList(page: number) {
    this.store.dispatch(initLoadRatedAnime({ page, limit: 36 }));
  }
}
