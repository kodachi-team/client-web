import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadNewAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.less'],
})
export class NewComponent implements OnInit, OnDestroy {
  public animeNew: any[] = [];
  public isLoadingAnimeNew: boolean = true;
  public isErrorAnimeNew: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(`New releases - Explorer ${environment.title}`);
    this.store.dispatch(initLoadNewAnime({ page: 0, limit: 36 }));
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.new_anime.data != null))
      .subscribe((data) => {
        this.animeNew = data.new_anime.data;
        this.isLoadingAnimeNew = data.new_anime.loading;
        this.isErrorAnimeNew = data.new_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateNewAnimeList(page: number) {
    this.store.dispatch(initLoadNewAnime({ page, limit: 36 }));
  }
}
