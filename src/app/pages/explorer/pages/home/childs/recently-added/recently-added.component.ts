import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadRecentAddedAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  templateUrl: './recently-added.component.html',
  styleUrls: ['./recently-added.component.less'],
})
export class RecentlyAddedComponent implements OnInit, OnDestroy {
  public animeRecentlyAdded: any[] = [];
  public isLoadingAnimeRecentlyAdded: boolean = true;
  public isErrorAnimeRecentlyAdded: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(`Recently added - Explorer ${environment.title}`);
    this.store.dispatch(initLoadRecentAddedAnime({ page: 0, limit: 36 }));
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.recent_added_anime.data != null))
      .subscribe((data) => {
        this.animeRecentlyAdded = data.recent_added_anime.data;
        this.isLoadingAnimeRecentlyAdded = data.recent_added_anime.loading;
        this.isErrorAnimeRecentlyAdded = data.recent_added_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateRecentlyAddedAnimeList(page: number) {
    this.store.dispatch(initLoadRecentAddedAnime({ page, limit: 36 }));
  }
}
