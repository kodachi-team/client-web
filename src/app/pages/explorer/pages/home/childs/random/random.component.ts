import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadRandomAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './random.component.html',
  styleUrls: ['./random.component.less'],
})
export class RandomComponent implements OnInit, OnDestroy {
  public animeRandom: any[] = [];
  public isLoadingAnimeRandom: boolean = true;
  public isErrorAnimeRandom: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(`New releases - Explorer ${environment.title}`);
    this.store.dispatch(initLoadRandomAnime({ page: 0, limit: 36 }));
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.random_anime.data != null))
      .subscribe((data) => {
        this.animeRandom = data.random_anime.data;
        this.isLoadingAnimeRandom = data.random_anime.loading;
        this.isErrorAnimeRandom = data.random_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateRandomAnimeList(page: number) {
    this.store.dispatch(initLoadRandomAnime({ page, limit: 36 }));
  }
}
