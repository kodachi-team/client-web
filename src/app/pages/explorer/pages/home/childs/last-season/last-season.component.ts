import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadLastSeasonAnime,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { environment } from '../../../../../../../environments/environment';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './last-season.component.html',
  styleUrls: ['./last-season.component.less'],
})
export class LastSeasonComponent implements OnInit, OnDestroy {
  public animeLastSeason: any[] = [];
  public isLoadingAnimeLastSeason: boolean = true;
  public isErrorAnimeLastSeason: boolean = false;

  public isAuth: boolean;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(`Last season - Explorer ${environment.title}`);
    this.store.dispatch(
      initLoadLastSeasonAnime({
        page: 0,
        limit: 24,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.last_season_anime.data != null))
      .subscribe((data) => {
        this.animeLastSeason = data.last_season_anime.data;
        this.isLoadingAnimeLastSeason = data.last_season_anime.loading;
        this.isErrorAnimeLastSeason = data.last_season_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();

    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }

  public paginateLastSeasonAnimeList(page: number) {
    this.store.dispatch(
      initLoadLastSeasonAnime({
        page,
        limit: 36,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
  }
}
