import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../../../../environments/environment';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../app-reducer.store';
import {
  initLoadNewAnime,
  initLoadLastSeasonAnime,
  initLoadRandomAnime,
  initLoadSeasonRatedAnime,
  initLoadRatedAnime,
  initLoadRecentAddedAnime,
  authActionsEnum,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
} from '../../../../../../store';

import { ofType, Actions } from '@ngrx/effects';

import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './sumary.component.html',
  styleUrls: ['./sumary.component.less'],
})
export class SumaryComponent implements OnInit, OnDestroy, AfterViewInit {
  public animeNew: any[] = [];
  public animeLastSeason: any[] = [];
  public animeRandom: any[] = [];
  public animeTopRatedAllTime: any[] = [];
  public animeTopRatedSeason: any[] = [];
  public animeLastAdded: any[] = [];

  public isLoadingAnimeNew: boolean = true;
  public isLoadingAnimeLastSeason: boolean = true;
  public isLoadingAnimeRandom: boolean = true;
  public isLoadingAnimeTopRatedSeason: boolean = true;
  public isLoadingAnimeTopRatedAllTime: boolean = true;
  public isLoadingAnimeLastAdded: boolean = true;

  public isErrorAnimeNew: boolean = false;
  public isErrorAnimeLastSeason: boolean = false;
  public isErrorAnimeRandom: boolean = false;
  public isErrorAnimeTopRatedSeason: boolean = false;
  public isErrorAnimeTopRatedAllTime: boolean = false;
  public isErrorAnimeLastAdded: boolean = false;

  public isAuth: boolean = false;

  public animeStoreSubscription: Subscription = new Subscription();
  public authStoreSubscription: Subscription = new Subscription();
  public authStateLogOutSubscription: Subscription = new Subscription();
  public authStateSignInSubscription: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private $actions: Actions,
    private title: Title,
  ) {
    this.authStateLogOutSubscription = this.$actions
      .pipe(ofType(authActionsEnum.LOG_OUT_ACCOUNT))
      .subscribe(() => {
        setTimeout(() => {
          this.isAuth = false;
        }, 1500);
      });

    this.authStateSignInSubscription = this.$actions
      .pipe(ofType(authActionsEnum.CHANGE_AUTH_STATE))
      .subscribe(() => {
        setTimeout(() => {
          this.isAuth = true;
        }, 1500);
      });
  }

  ngOnInit(): void {
    this.title.setTitle(`Explorer ${environment.title}`);
    this.animeStoreSubscription = this.store
      .select('anime')
      .pipe(filter((data) => data.new_anime.data != null))
      .subscribe((data) => {
        this.animeNew = data.new_anime.data;
        this.animeLastSeason = data.last_season_anime.data;
        this.animeRandom = data.random_anime.data;
        this.animeTopRatedSeason = data.rated_season_anime.data;
        this.animeTopRatedAllTime = data.rated_anime.data;
        this.animeLastAdded = data.recent_added_anime.data;

        this.isLoadingAnimeNew = data.new_anime.loading;
        this.isLoadingAnimeLastSeason = data.last_season_anime.loading;
        this.isLoadingAnimeRandom = data.random_anime.loading;
        this.isLoadingAnimeTopRatedSeason = data.rated_season_anime.loading;
        this.isLoadingAnimeTopRatedAllTime = data.rated_anime.loading;
        this.isLoadingAnimeLastAdded = data.recent_added_anime.loading;

        this.isErrorAnimeNew = data.new_anime.error;
        this.isErrorAnimeLastSeason = data.last_season_anime.error;
        this.isErrorAnimeRandom = data.random_anime.error;
        this.isErrorAnimeTopRatedSeason = data.rated_season_anime.error;
        this.isErrorAnimeTopRatedAllTime = data.rated_anime.error;
        this.isErrorAnimeLastAdded = data.recent_added_anime.error;
      });

    this.authStoreSubscription = this.store.select('auth').subscribe((data) => {
      this.isAuth = data.session;
    });
  }

  ngAfterViewInit(): void {
    this.loadActions();
  }

  private loadActions() {
    this.store.dispatch(initLoadNewAnime({ page: 0, limit: 24 }));
    this.store.dispatch(
      initLoadLastSeasonAnime({
        page: 0,
        limit: 24,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
    this.store.dispatch(initLoadRandomAnime({ page: 0, limit: 24 }));
    this.store.dispatch(
      initLoadSeasonRatedAnime({
        page: 0,
        limit: 24,
        startDate: '2010-01-01T00:00:00.000Z',
        endDate: '2020-01-01T00:00:00.000Z',
      }),
    );
    this.store.dispatch(initLoadRatedAnime({ page: 0, limit: 24 }));
    this.store.dispatch(initLoadRecentAddedAnime({ page: 0, limit: 24 }));
  }

  ngOnDestroy(): void {
    this.animeStoreSubscription.unsubscribe();
    this.authStateLogOutSubscription.unsubscribe();
    this.authStateSignInSubscription.unsubscribe();
    this.authStoreSubscription.unsubscribe();
    if (this.isAuth) {
      this.store.dispatch(resetStateListAnimeAuthAction());
    } else {
      this.store.dispatch(resetStateListAnimeNoAuthAction());
    }
  }
}
