import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExplorerComponent } from './explorer.component';
import { HomeComponent } from './pages/home/home.component';
import { ViewComponent } from './pages/view/view.component';
import { SumaryComponent } from './pages/home/childs/sumary/sumary.component';
import { NewComponent } from './pages/home/childs/new/new.component';
import { LastSeasonComponent } from './pages/home/childs/last-season/last-season.component';
import { TopRatedComponent } from './pages/home/childs/top-rated/top-rated.component';
import { RandomComponent } from './pages/home/childs/random/random.component';
import { TopRatedAllTimeComponent } from './pages/home/childs/top-rated-all-time/top-rated-all-time.component';
import { RecentlyAddedComponent } from './pages/home/childs/recently-added/recently-added.component';
import { ByCategoryComponent } from './pages/home/childs/by-category/by-category.component';

const routes: Routes = [
  {
    path: '',
    component: ExplorerComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        children: [
          {
            path: 'sumary',
            component: SumaryComponent,
          },
          {
            path: 'new',
            component: NewComponent,
          },
          {
            path: 'last-season',
            component: LastSeasonComponent,
          },
          {
            path: 'top-rated',
            component: TopRatedComponent,
          },
          {
            path: 'random',
            component: RandomComponent,
          },
          {
            path: 'top-rated-all-time',
            component: TopRatedAllTimeComponent,
          },
          {
            path: 'recently-added',
            component: RecentlyAddedComponent,
          },
          {
            path: 'category/:category',
            component: ByCategoryComponent,
          },
          {
            path: '',
            redirectTo: 'sumary',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'view/:slug',
        component: ViewComponent,
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExplorerRoutingModule {}
