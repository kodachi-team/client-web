import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AnimeEffects } from '../../store/anime/anime.effect';

import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';

import { ExplorerRoutingModule } from './explorer-routing.module';
import { ExplorerComponent } from './explorer.component';
import { HomeComponent } from './pages/home/home.component';
import { ViewComponent } from './pages/view/view.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { SidebarCategoryComponent } from './components/sidebar-category/sidebar-category.component';
import { CategoryComponent } from './components/category/category.component';
import { AnimeComponent } from './components/anime/anime.component';
import { SumaryComponent } from './pages/home/childs/sumary/sumary.component';
import { NewComponent } from './pages/home/childs/new/new.component';
import { LastSeasonComponent } from './pages/home/childs/last-season/last-season.component';
import { TopRatedComponent } from './pages/home/childs/top-rated/top-rated.component';
import { RandomComponent } from './pages/home/childs/random/random.component';
import { TopRatedAllTimeComponent } from './pages/home/childs/top-rated-all-time/top-rated-all-time.component';
import { RecentlyAddedComponent } from './pages/home/childs/recently-added/recently-added.component';
import { ByCategoryComponent } from './pages/home/childs/by-category/by-category.component';

@NgModule({
  declarations: [
    ExplorerComponent,
    HomeComponent,
    ViewComponent,
    CarouselComponent,
    SidebarCategoryComponent,
    CategoryComponent,
    AnimeComponent,
    SumaryComponent,
    NewComponent,
    LastSeasonComponent,
    TopRatedComponent,
    RandomComponent,
    TopRatedAllTimeComponent,
    RecentlyAddedComponent,
    ByCategoryComponent,
  ],
  imports: [
    CommonModule,
    ExplorerRoutingModule,
    RouterModule,
    FormsModule,
    FlexLayoutModule,
    StoreModule,
    EffectsModule.forFeature([AnimeEffects]),
    NzCarouselModule,
    NzLayoutModule,
    NzButtonModule,
    NzIconModule,
    NzDividerModule,
    NzRateModule,
    NzMenuModule,
    NzGridModule,
    NzTypographyModule,
    NzPopoverModule,
    NzStatisticModule,
    NzSkeletonModule,
    NzEmptyModule,
    NzPaginationModule,
  ],
})
export class ExplorerModule {}
