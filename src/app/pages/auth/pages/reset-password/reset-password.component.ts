import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';
import * as ip from 'public-ip';

import { AuthService } from '../../../../utils/services/auth/auth.service';
import { environment } from '../../../../../environments/environment';

@Component({
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less'],
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  public selectedStep: number = 1;

  public recoverEmailForm: FormGroup;
  public recoverPasswordForm: FormGroup;

  public authSubscribe: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private notification: NzNotificationService,
    private router: Router,
    private title: Title,
  ) {
    this.recoverEmailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

    this.recoverPasswordForm = this.formBuilder.group({
      token: ['', Validators.required],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`Reset password ${environment.subtitle}`);
  }

  ngOnDestroy(): void {
    this.authSubscribe.unsubscribe();
  }

  public changeStep(type: number) {
    if (type == 1) {
      if (this.selectedStep == 2) {
        this.selectedStep = 2;
      } else {
        this.selectedStep = this.selectedStep + 1;
      }
    } else if (type == 0) {
      if (this.selectedStep == 1) {
        this.selectedStep = 1;
      } else {
        this.selectedStep = this.selectedStep - 1;
      }
    }
  }

  public async sendPasswordReset() {
    if (this.recoverEmailForm.invalid) {
      return this.notification.create(
        'error',
        "Why don't you listen to us? ( ͒˃⌂˂ ͒)",
        'We need you to meet all the requirements to continue.',
      );
    }

    const data = {
      userEmail: this.recoverEmailForm.controls.email.value,
      userIp: await ip.v4(),
    };

    this.recoverEmailForm.disable();

    this.authSubscribe = this._authService
      .requestPasswordChange(data)
      .subscribe({
        next: (data: any) => {
          this.notification.create(
            'success',
            'All ready ┌(▀Ĺ̯ ▀-͠ )┐',
            data.message,
          );
        },
        error: (data: any) => {
          this.recoverEmailForm.enable();

          this.notification.create(
            'error',
            'We have problems (▰˘︹˘▰)',
            data.error.message,
          );
        },
        complete: () => {
          this.selectedStep = 2;
        },
      });
  }

  public changePassword() {
    if (this.recoverPasswordForm.invalid) {
      return this.notification.create(
        'error',
        "Why don't you listen to us? ( ͒˃⌂˂ ͒)",
        'We need you to meet all the requirements to continue.',
      );
    }

    if (
      this.recoverPasswordForm.controls.password.value !==
      this.recoverPasswordForm.controls.password_confirm.value
    ) {
      return this.notification.create(
        'error',
        "Why don't you listen to us? ( ͒˃⌂˂ ͒)",
        'Please confirm that your passwords are the same.',
      );
    }

    const data = {
      userEmail: this.recoverEmailForm.controls.email.value,
      tokenPassword: this.recoverPasswordForm.controls.token.value,
      newPassword: this.recoverPasswordForm.controls.password_confirm.value,
    };

    this.recoverEmailForm.disable();
    this.recoverPasswordForm.disable();

    this.authSubscribe = this._authService
      .changePasswordAccount(data)
      .subscribe({
        next: (data: any) => {
          this.notification.create(
            'success',
            'All ready ┌(▀Ĺ̯ ▀-͠ )┐',
            data.message,
          );
        },
        error: (data: any) => {
          this.recoverEmailForm.enable();
          this.recoverPasswordForm.enable();

          this.notification.create(
            'error',
            'We have problems (▰˘︹˘▰)',
            data.error.message,
          );
        },
        complete: () => {
          this.router.navigate(['/auth/sign-in']);
        },
      });
  }
}
