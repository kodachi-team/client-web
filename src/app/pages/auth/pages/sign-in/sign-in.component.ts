import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';
import { AppState } from '../../../../app-reducer.store';
import { initChangeAuthState } from '../../../../store';

import { NzNotificationService } from 'ng-zorro-antd';

import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';
import * as ip from 'public-ip';

import { AuthService } from '../../../../utils/services/auth/auth.service';
import { SignInI } from '../../../../utils/interfaces/auth/signin.interface';
import { environment } from '../../../../../environments/environment';

@Component({
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less'],
})
export class SignInComponent implements OnInit, OnDestroy {
  public signInForm: FormGroup;

  private authSignInSubscribe: Subscription = new Subscription();
  private authStoreSubscribe: Subscription = new Subscription();

  public isError: boolean = false;
  public messageError: string;
  public isLogged: boolean = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private notification: NzNotificationService,
    private title: Title,
    private store: Store<AppState>,
    private deviceDetector: DeviceDetectorService,
    private router: Router,
  ) {
    this.signInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      remember: null,
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`Sign In ${environment.subtitle}`);

    if (localStorage.getItem('email') && localStorage.getItem('password')) {
      this.signInForm.controls.email.setValue(localStorage.getItem('email'));
      this.signInForm.controls.password.setValue(
        localStorage.getItem('password'),
      );
      this.signInForm.controls.remember.setValue(true);
    } else {
      this.signInForm.controls.remember.setValue(true);
    }

    this.authStoreSubscribe = this.store.select('auth').subscribe((data) => {
      this.isError = data.error;
      this.messageError = data.message;
      this.isLogged = data.session;
    });
  }

  ngOnDestroy(): void {
    this.authSignInSubscribe.unsubscribe();
    this.authStoreSubscribe.unsubscribe();
  }

  public async signIn() {
    if (this.signInForm.invalid) {
      this.signInForm.enable();
      return this.notification.create(
        'warning',
        'Some things are missing (╥﹏╥)',
        'Please complete the required fields.',
      );
    }

    this.signInForm.disable();
    if (this.signInForm.controls.remember.value) {
      localStorage.setItem('email', this.signInForm.controls.email.value);
      localStorage.setItem('password', this.signInForm.controls.password.value);
    } else {
      localStorage.removeItem('email');
      localStorage.removeItem('password');
    }

    const data: SignInI = {
      email: this.signInForm.controls.email.value,
      password: this.signInForm.controls.password.value,
      device: this.deviceDetector.getDeviceInfo().os,
      ip: await ip.v4(),
    };

    this.authSignInSubscribe = this.authService.signInAccount(data).subscribe({
      next: (data: any) => {
        localStorage.setItem('id', data.user_id);
        localStorage.setItem('session', data.session_id);
        localStorage.setItem('token', data.token);
        this.store.dispatch(initChangeAuthState({ userId: data.user_id }));
      },
      error: (data: any) => {
        this.signInForm.enable();
        this.notification.create(
          'error',
          'We have failed you (▰˘︹˘▰)',
          data.error.message,
        );
      },
      complete: () => {
        setTimeout(() => {
          if (this.isError) {
            localStorage.removeItem('id');
            localStorage.removeItem('session');
            localStorage.removeItem('token');
            this.signInForm.enable();
            return this.notification.create(
              'error',
              'We have failed you (▰˘︹˘▰)',
              this.messageError,
            );
          }

          this.notification.create(
            'success',
            'We have good news ( ͡° ͜ʖ ͡°)',
            'You have successfully logged in, welcome.',
          );

          this.router.navigate(['/explorer']);
        }, 1500);
      },
    });
  }
}
