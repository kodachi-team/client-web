import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { NzNotificationService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';

import { AuthService } from '../../../../utils/services/auth/auth.service';
import { environment } from '../../../../../environments/environment';

@Component({
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.less'],
})
export class ActivateAccountComponent implements OnInit, OnDestroy {
  public confirmForm: FormGroup;

  private authSignUpSubscription: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private title: Title,
    private notification: NzNotificationService,
    private router: Router,
  ) {
    this.confirmForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      token: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`Activate account ${environment.subtitle}`);
  }

  ngOnDestroy(): void {
    this.authSignUpSubscription.unsubscribe();
  }

  public confirmAccount() {
    if (this.confirmForm.invalid) {
      return this.notification.create(
        'info',
        'Some things are missing (✖╭╮✖)',
        'Please complete the form to continue. (ﾉ´ｰ`)ﾉ',
      );
    }

    const data = {
      email: this.confirmForm.controls.email.value,
      token: this.confirmForm.controls.token.value,
    };

    this.confirmForm.disable();

    this.authSignUpSubscription = this._authService
      .activateAccount(data)
      .subscribe({
        next: (response: any) => {
          this.notification.create(
            'success',
            'All ready (⌐■_■)',
            response.message,
          );

          this.router.navigate(['/auth/sign-in']);
        },
        error: (response: any) => {
          this.confirmForm.enable();
          this.notification.create(
            'error',
            'We have problems (▰˘︹˘▰)',
            response.error.message,
          );
        },
      });
  }
}
