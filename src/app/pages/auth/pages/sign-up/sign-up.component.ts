import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { NzNotificationService } from 'ng-zorro-antd';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';
import * as ip from 'public-ip';

import { AnimeService } from '../../../../utils/services/anime/anime.service';
import { AuthService } from '../../../../utils/services/auth/auth.service';
import { environment } from '../../../../../environments/environment';

@Component({
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less'],
})
export class SignUpComponent implements OnInit, OnDestroy {
  public selectedStep: number = 1;

  public categoryAnimeList: string[];
  public selectedCategoryAnimesDefault: string[];
  public selectedCategoryAnimes: string[] = [];

  public accountForm: FormGroup;
  public profileForm: FormGroup;
  public confirmForm: FormGroup;

  private authSignUpSubscription: Subscription = new Subscription();

  constructor(
    private _animeService: AnimeService,
    private _authService: AuthService,
    private formBuilder: FormBuilder,
    private notification: NzNotificationService,
    private title: Title,
    private router: Router,
    private deviceDetector: DeviceDetectorService,
  ) {
    this.categoryAnimeList = _animeService.getCategoryAnimeList();

    this.accountForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required],
    });

    this.profileForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(15),
        ],
      ],
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(30),
        ],
      ],
    });

    this.confirmForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      token: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`Sign Up ${environment.subtitle}`);
  }

  ngOnDestroy(): void {
    this.authSignUpSubscription.unsubscribe();
  }

  public changeStep(type: number) {
    if (type == 1) {
      if (this.selectedStep == 3) {
        this.selectedStep = 3;
      } else {
        this.selectedStep = this.selectedStep + 1;
      }
    } else if (type == 0) {
      if (this.selectedStep == 1) {
        this.selectedStep = 1;
      } else {
        this.selectedStep = this.selectedStep - 1;
      }
    }
  }

  public getCategoriesSelected(event: string[]) {
    this.selectedCategoryAnimes = event;
  }

  public async createAccount() {
    if (
      this.accountForm.controls.password.value !==
      this.accountForm.controls.password_confirm.value
    ) {
      return this.notification.create(
        'error',
        'Bad passwords (;¬_¬)',
        'Please confirm that the passwords are the same. <(｀^´)>',
      );
    }

    if (this.accountForm.invalid || this.profileForm.invalid) {
      return this.notification.create(
        'error',
        "You don't follow the rules (;¬_¬)",
        'If you do not fulfill what we ask for, you will not have an account, be careful. <(｀^´)>',
      );
    }

    this.accountForm.disable();
    this.profileForm.disable();

    const data = {
      email: {
        key: this.accountForm.controls.email.value,
      },
      name: this.profileForm.controls.name.value,
      username: `@${new String(this.profileForm.controls.username.value)
        .toLowerCase()
        .replace('@', '')}`,
      device: this.deviceDetector.getDeviceInfo().os,
      categories: this.selectedCategoryAnimes,
      ip: await ip.v4(),
      password: {
        key: this.accountForm.controls.password_confirm.value,
      },
    };

    this.authSignUpSubscription = this._authService
      .signUpAccount(data)
      .subscribe({
        next: (data: any) => {
          this.notification.create(
            'success',
            'We have good news ( ͡° ͜ʖ ͡°)',
            data.message,
          );
        },
        error: (data: any) => {
          this.accountForm.enable();
          this.profileForm.enable();
          this.notification.create(
            'error',
            'We have failed you (▰˘︹˘▰)',
            data.error.message,
          );
        },
        complete: () => {
          this.selectedStep = 3;
        },
      });
  }

  public confirmAccount() {
    if (this.confirmForm.invalid) {
      return this.notification.create(
        'info',
        'Some things are missing (✖╭╮✖)',
        'Please complete the form to continue. (ﾉ´ｰ`)ﾉ',
      );
    }

    const data = {
      email: this.confirmForm.controls.email.value,
      token: this.confirmForm.controls.token.value,
    };

    this.confirmForm.disable();

    this.authSignUpSubscription = this._authService
      .activateAccount(data)
      .subscribe({
        next: (response: any) => {
          this.notification.create(
            'success',
            'All ready (⌐■_■)',
            response.message,
          );

          this.router.navigate(['/auth/sign-in']);
        },
        error: (response: any) => {
          this.confirmForm.enable();
          this.notification.create(
            'error',
            'We have problems (▰˘︹˘▰)',
            response.error.message,
          );
        },
      });
  }
}
