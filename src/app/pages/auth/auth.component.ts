import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.less'],
})
export class AuthComponent implements OnInit {
  public images: string[] = [
    '../../../assets/auth/bg-1.jpg',
    '../../../assets/auth/bg-2.jpg',
    '../../../assets/auth/bg-4.jpg',
  ];
  public imgBg: string = this.images[(Math.random() * this.images.length) | 0];

  constructor() {
    this.changeImgBg();
  }

  ngOnInit(): void {}

  changeImgBg() {
    setTimeout(() => {
      this.imgBg = this.images[(Math.random() * this.images.length) | 0];
      this.changeImgBg();
    }, 5000);
  }
}
