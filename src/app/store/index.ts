// Settings store
export * from './settings/settings.action';
export * from './settings/settings.reducer';

// Auth store
export * from './auth/auth.action';
export * from './auth/auth.reducer';

// Anime store
export * from './anime/anime.action';
export * from './anime/anime.reducer';

// Config Effects
import { AuthEffects } from './auth/auth.effect';
import { AnimeEffects } from './anime/anime.effect';

export const Effects: any[] = [AuthEffects, AnimeEffects];
