import { createAction, props } from '@ngrx/store';

export const setVisibilityHeaderFooter = createAction(
  '[Settings] Visibility established for the header and footer.',
  props<{ visibility: boolean }>(),
);
