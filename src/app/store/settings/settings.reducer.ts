import { createReducer, on, Action } from '@ngrx/store';
import { setVisibilityHeaderFooter } from './settings.action';

export interface settingsStateI {
  showHeaderFooter: boolean;
  notifications: boolean;
}

export const settingsState: settingsStateI = {
  showHeaderFooter: false,
  notifications: false,
};

const _settigsReducer = createReducer(
  settingsState,
  on(setVisibilityHeaderFooter, (state, { visibility }) => ({
    ...state,
    showHeaderFooter: visibility,
  })),
);

export function settingsReducer(state: settingsStateI, action: Action) {
  return _settigsReducer(state, action);
}
