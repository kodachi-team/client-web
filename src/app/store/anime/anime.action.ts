import { createAction, props } from '@ngrx/store';

export const initLoadNewAnime = createAction(
  '[Anime] Starting process to load the list of new anime.',
  props<{
    page: number;
    limit: number;
  }>(),
);

export const addAnimeNewAction = createAction(
  '[Anime] Adding list of new anime.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeNewAction = createAction(
  '[Anime] Error getting the list of new anime.',
  props<{
    message: any;
  }>(),
);

export const initLoadRecomendationAnime = createAction(
  '[Anime] Starting process to obtain the recommended anime for the user.',
  props<{
    page: number;
    limit: number;
    userId: string;
  }>(),
);

export const addAnimeRecomendationAction = createAction(
  '[Anime] Adding list of recommended anime for the user.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeRecomendationAction = createAction(
  '[Anime] Error getting the list of recommended anime for the user.',
  props<{
    message: any;
  }>(),
);

export const initLoadLastSeasonAnime = createAction(
  '[Anime] Starting process to obtain the list of the anime from the previous season.',
  props<{
    page: number;
    limit: number;
    startDate: string;
    endDate: string;
  }>(),
);

export const addAnimeLastSeasonAction = createAction(
  '[Anime] Adding list of the anime from the previous season.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeLastSeasonAction = createAction(
  '[Anime] Error getting the list of the anime from the previous season.',
  props<{
    message: any;
  }>(),
);

export const initLoadRandomAnime = createAction(
  '[Anime] Starting process to get the list of random anime.',
  props<{
    page: number;
    limit: number;
  }>(),
);

export const addAnimeRandomAction = createAction(
  '[Anime] Adding list of the random anime.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeRandomAction = createAction(
  '[Anime] Error getting the list of random anime.',
  props<{
    message: any;
  }>(),
);

export const initLoadSeasonRatedAnime = createAction(
  '[Anime] Starting process to get the best anime of the season.',
  props<{
    page: number;
    limit: number;
    startDate: string;
    endDate: string;
  }>(),
);

export const addAnimeSeasonRatedAction = createAction(
  '[Anime] Adding list of best anime of the season.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeSeasonRatedAction = createAction(
  '[Anime] Error getting the list of the best anime of the season.',
  props<{
    message: any;
  }>(),
);

export const initLoadRatedAnime = createAction(
  '[Anime] Starting process to load the list of the best rated anime.',
  props<{
    page: number;
    limit: number;
  }>(),
);

export const addAnimeRatedAction = createAction(
  '[Anime] Adding list of best rated anime.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeRatedAction = createAction(
  '[Anime] Error getting the list of the best evaluated anime.',
  props<{
    message: any;
  }>(),
);

export const initLoadRecentAddedAnime = createAction(
  '[Anime] Starting process to get the list of recently added anime.',
  props<{
    page: number;
    limit: number;
  }>(),
);

export const addAnimeRecentAddedAction = createAction(
  '[Anime] Adding list of recently added anime.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeRecentAddedAction = createAction(
  '[Anime] Error getting the list of the recently added anime.',
  props<{
    message: any;
  }>(),
);

export const initLoadAnimeCategoryAnime = createAction(
  '[Anime] Starting process to add list of anime by a selected category.',
  props<{
    page: number;
    limit: number;
    category: number;
  }>(),
);

export const addAnimeAnimeCategoryAction = createAction(
  '[Anime] Adding list of anime by a selected category.',
  props<{
    data: any;
  }>(),
);

export const errorAddAnimeCategoryAction = createAction(
  '[Anime] Error getting the list of anime by a selected category.',
  props<{
    message: any;
  }>(),
);

export const resetStateListAnimeAuthAction = createAction(
  '[Anime-Auth] Resetting status of the anime list.',
);

export const resetStateListAnimeNoAuthAction = createAction(
  '[Anime-NoAuth] Resetting status of the anime list.',
);

export enum animeActionsEnum {
  INIT_LOAD_NEW_ANIME = '[Anime] Starting process to load the list of new anime.',
  INIT_LOAD_RECOMENDATION_ANIME = '[Anime] Starting process to obtain the recommended anime for the user.',
  INIT_LOAD_LAST_SEASON_ANIME = '[Anime] Starting process to obtain the list of the anime from the previous season.',
  INIT_LOAD_RANDOM_ANIME = '[Anime] Starting process to get the list of random anime.',
  INIT_LOAD_RATED_SEASON_ANIME = '[Anime] Starting process to get the best anime of the season.',
  INIT_LOAD_RATED_ANIME = '[Anime] Starting process to load the list of the best rated anime.',
  INIT_LOAD_RECENT_ADDED_ANIME = '[Anime] Starting process to get the list of recently added anime.',
  INIT_LOAD_CATEGORY_ANIME = '[Anime] Starting process to add list of anime by a selected category.',
}
