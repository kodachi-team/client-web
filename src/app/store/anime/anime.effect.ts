import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';

import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { AnimeService } from '../../utils/services/anime/anime.service';
import {
  animeActionsEnum,
  errorAddAnimeRatedAction,
  addAnimeRatedAction,
  addAnimeNewAction,
  errorAddAnimeNewAction,
  addAnimeLastSeasonAction,
  errorAddAnimeLastSeasonAction,
  addAnimeRandomAction,
  errorAddAnimeRandomAction,
  addAnimeSeasonRatedAction,
  errorAddAnimeSeasonRatedAction,
  addAnimeRecentAddedAction,
  errorAddAnimeRecentAddedAction,
  addAnimeRecomendationAction,
  errorAddAnimeRecomendationAction,
  addAnimeAnimeCategoryAction,
  errorAddAnimeCategoryAction,
} from './anime.action';

@Injectable()
export class AnimeEffects {
  constructor(readonly animeService: AnimeService, private actions$: Actions) {}

  loadNewAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_NEW_ANIME),
      mergeMap((action: any) =>
        this.animeService.getNewAnime(action.page, action.limit).pipe(
          map((response: any) =>
            addAnimeNewAction({
              data: response,
            }),
          ),
          catchError((error) =>
            of(
              errorAddAnimeNewAction({
                message: error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  loadRecomendationAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_RECOMENDATION_ANIME),
      mergeMap((action: any) =>
        this.animeService
          .getRecomendationAnime(action.page, action.limit, action.userId)
          .pipe(
            map((response: any) =>
              addAnimeRecomendationAction({
                data: response,
              }),
            ),
            catchError((error) =>
              of(
                errorAddAnimeRecomendationAction({
                  message: error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );

  loadLastSeasonAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_LAST_SEASON_ANIME),
      mergeMap((action: any) =>
        this.animeService
          .getLastSeasonAnime(
            action.page,
            action.limit,
            action.startDate,
            action.endDate,
          )
          .pipe(
            map((response: any) =>
              addAnimeLastSeasonAction({
                data: response,
              }),
            ),
            catchError((error) =>
              of(
                errorAddAnimeLastSeasonAction({
                  message: error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );

  loadRandomAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_RANDOM_ANIME),
      mergeMap((action: any) =>
        this.animeService.getRandomAnime(action.limit).pipe(
          map((response: any) =>
            addAnimeRandomAction({
              data: response,
            }),
          ),
          catchError((error) =>
            of(
              errorAddAnimeRandomAction({
                message: error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  loadRatedSeasonAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_RATED_SEASON_ANIME),
      mergeMap((action: any) =>
        this.animeService
          .getRatedSeasonAnime(
            action.page,
            action.limit,
            action.startDate,
            action.endDate,
          )
          .pipe(
            map((response: any) =>
              addAnimeSeasonRatedAction({
                data: response,
              }),
            ),
            catchError((error) =>
              of(
                errorAddAnimeSeasonRatedAction({
                  message: error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );

  loadRatedAllTimeAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_RATED_ANIME),
      mergeMap((action: any) =>
        this.animeService.getRatedAnimeAllTime(action.page, action.limit).pipe(
          map((response: any) =>
            addAnimeRatedAction({
              data: response,
            }),
          ),
          catchError((error) =>
            of(
              errorAddAnimeRatedAction({
                message: error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  loadRecentAddedAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_RECENT_ADDED_ANIME),
      mergeMap((action: any) =>
        this.animeService.getRecentAdded(action.page, action.limit).pipe(
          map((response: any) =>
            addAnimeRecentAddedAction({
              data: response,
            }),
          ),
          catchError((error) =>
            of(
              errorAddAnimeRecentAddedAction({
                message: error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  loadCategoryAnime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(animeActionsEnum.INIT_LOAD_CATEGORY_ANIME),
      mergeMap((action: any) =>
        this.animeService
          .getByCategory(action.page, action.limit, action.category)
          .pipe(
            map((response: any) =>
              addAnimeAnimeCategoryAction({
                data: response,
              }),
            ),
            catchError((error) =>
              of(
                errorAddAnimeCategoryAction({
                  message: error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );
}
