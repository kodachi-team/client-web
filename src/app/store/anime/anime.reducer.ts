import { createReducer, on, Action } from '@ngrx/store';
import {
  initLoadRatedAnime,
  addAnimeRatedAction,
  errorAddAnimeRatedAction,
  initLoadNewAnime,
  addAnimeNewAction,
  errorAddAnimeNewAction,
  initLoadLastSeasonAnime,
  addAnimeLastSeasonAction,
  errorAddAnimeLastSeasonAction,
  initLoadRecomendationAnime,
  addAnimeRecomendationAction,
  errorAddAnimeRecomendationAction,
  initLoadRandomAnime,
  addAnimeRandomAction,
  errorAddAnimeRandomAction,
  initLoadSeasonRatedAnime,
  addAnimeSeasonRatedAction,
  errorAddAnimeSeasonRatedAction,
  errorAddAnimeRecentAddedAction,
  initLoadRecentAddedAnime,
  addAnimeRecentAddedAction,
  resetStateListAnimeAuthAction,
  resetStateListAnimeNoAuthAction,
  initLoadAnimeCategoryAnime,
  addAnimeAnimeCategoryAction,
  errorAddAnimeCategoryAction,
} from './anime.action';

interface animeDataCategory {
  data: any;
  message: string;
  error: boolean;
  loading: boolean;
  category: number;
}

interface animeData {
  data: any;
  message: string;
  error: boolean;
  loading: boolean;
}

export interface animeStateI {
  new_anime: animeData;
  recomendation_anime: animeData;
  last_season_anime: animeData;
  random_anime: animeData;
  rated_season_anime: animeData;
  rated_anime: animeData;
  recent_added_anime: animeData;
  category_anime: animeDataCategory;
}

export const animeInitState: animeStateI = {
  new_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  recomendation_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  last_season_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  random_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  rated_season_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  rated_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  recent_added_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
  },
  category_anime: {
    data: null,
    message: null,
    error: false,
    loading: false,
    category: 0,
  },
};

const _animeReducer = createReducer(
  animeInitState,
  on(initLoadNewAnime, (state) => ({
    ...state,
    new_anime: {
      data: state.new_anime.data,
      message: state.new_anime.message,
      error: state.new_anime.error,
      loading: true,
    },
  })),
  on(addAnimeNewAction, (state, { data }) => ({
    ...state,
    new_anime: {
      data,
      message: state.new_anime.message,
      error: state.new_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeNewAction, (state, { message }) => ({
    ...state,
    new_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadLastSeasonAnime, (state) => ({
    ...state,
    last_season_anime: {
      data: state.last_season_anime.data,
      message: state.last_season_anime.message,
      error: state.last_season_anime.error,
      loading: true,
    },
  })),
  on(addAnimeLastSeasonAction, (state, { data }) => ({
    ...state,
    last_season_anime: {
      data,
      message: state.last_season_anime.message,
      error: state.last_season_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeLastSeasonAction, (state, { message }) => ({
    ...state,
    last_season_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadRecomendationAnime, (state) => ({
    ...state,
    recomendation_anime: {
      data: state.recomendation_anime.data,
      message: state.recomendation_anime.message,
      error: state.recomendation_anime.error,
      loading: true,
    },
  })),
  on(addAnimeRecomendationAction, (state, { data }) => ({
    ...state,
    recomendation_anime: {
      data,
      message: state.recomendation_anime.message,
      error: state.recomendation_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeRecomendationAction, (state, { message }) => ({
    ...state,
    recomendation_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadRandomAnime, (state) => ({
    ...state,
    random_anime: {
      data: state.random_anime.data,
      message: state.random_anime.message,
      error: state.random_anime.error,
      loading: true,
    },
  })),
  on(addAnimeRandomAction, (state, { data }) => ({
    ...state,
    random_anime: {
      data,
      message: state.random_anime.message,
      error: state.random_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeRandomAction, (state, { message }) => ({
    ...state,
    random_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadSeasonRatedAnime, (state) => ({
    ...state,
    rated_season_anime: {
      data: state.rated_season_anime.data,
      message: state.rated_season_anime.message,
      error: state.rated_season_anime.error,
      loading: true,
    },
  })),
  on(addAnimeSeasonRatedAction, (state, { data }) => ({
    ...state,
    rated_season_anime: {
      data,
      message: state.rated_season_anime.message,
      error: state.rated_season_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeSeasonRatedAction, (state, { message }) => ({
    ...state,
    rated_season_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadRatedAnime, (state) => ({
    ...state,
    rated_anime: {
      data: state.rated_anime.data,
      message: state.rated_anime.message,
      error: state.rated_anime.error,
      loading: true,
    },
  })),
  on(addAnimeRatedAction, (state, { data }) => ({
    ...state,
    rated_anime: {
      data,
      message: state.rated_anime.message,
      error: state.rated_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeRatedAction, (state, { message }) => ({
    ...state,
    rated_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadRecentAddedAnime, (state) => ({
    ...state,
    recent_added_anime: {
      data: state.recent_added_anime.data,
      message: state.recent_added_anime.message,
      error: state.recent_added_anime.error,
      loading: true,
    },
  })),
  on(addAnimeRecentAddedAction, (state, { data }) => ({
    ...state,
    recent_added_anime: {
      data,
      message: state.recent_added_anime.message,
      error: state.recent_added_anime.error,
      loading: false,
    },
  })),
  on(errorAddAnimeRecentAddedAction, (state, { message }) => ({
    ...state,
    recent_added_anime: {
      data: null,
      message,
      error: true,
      loading: false,
    },
  })),
  on(initLoadAnimeCategoryAnime, (state, { category }) => ({
    ...state,
    category_anime: {
      data: state.category_anime.data,
      message: state.category_anime.message,
      error: state.category_anime.error,
      loading: true,
      category,
    },
  })),
  on(addAnimeAnimeCategoryAction, (state, { data }) => ({
    ...state,
    category_anime: {
      data,
      message: state.category_anime.message,
      error: state.category_anime.error,
      loading: false,
      category: state.category_anime.category,
    },
  })),
  on(errorAddAnimeCategoryAction, (state, { message }) => ({
    ...state,
    category_anime: {
      data: null,
      message,
      error: true,
      loading: false,
      category: 0,
    },
  })),
  on(resetStateListAnimeAuthAction, (state) => ({
    ...state,
    new_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    last_season_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    random_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    rated_season_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    rated_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    recent_added_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    category_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
      category: 0,
    },
  })),
  on(resetStateListAnimeNoAuthAction, (state) => ({
    ...state,
    new_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    recomendation_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    last_season_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    random_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    rated_season_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    recent_added_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
    },
    category_anime: {
      data: null,
      message: null,
      error: false,
      loading: false,
      category: 0,
    },
  })),
);

export function animeReducer(state: animeStateI, action: Action) {
  return _animeReducer(state, action);
}
