import { createAction, props } from '@ngrx/store';

export const initChangeAuthState = createAction(
  "[Auth] Initiating process to change the user's authentication status.",
  props<{
    userId: string;
  }>(),
);

export const initSignOutAction = createAction(
  '[Auth] Starting process to close the user session.',
  props<{
    userId: string;
    sessionId: string;
  }>(),
);

export const changeAuthState = createAction(
  '[Auth] Changing user authentication status.',
  props<{
    user: any;
    session: boolean;
    suspension: boolean;
    error: boolean;
    message: string;
  }>(),
);

export const changeAuthStateError = createAction(
  '[Auth] Failed to change user authentication status.',
  props<{
    message: string;
  }>(),
);

export const logOutAccountAction = createAction(
  '[Auth] Closing user session and cleaning data.',
  props<{
    message: string;
  }>(),
);

export enum authActionsEnum {
  INIT_SIGN_IN = "[Auth] Initiating process to change the user's authentication status.",
  INIT_SIGN_OUT = '[Auth] Starting process to close the user session.',
  CHANGE_AUTH_STATE = '[Auth] Changing user authentication status.',
  LOG_OUT_ACCOUNT = '[Auth] Closing user session and cleaning data.',
}
