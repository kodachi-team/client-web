import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';

import { map, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  authActionsEnum,
  changeAuthState,
  changeAuthStateError,
  logOutAccountAction,
} from './auth.action';
import { AuthService } from '../../utils/services/auth/auth.service';

@Injectable()
export class AuthEffects {
  constructor(private _authService: AuthService, private actions$: Actions) {}

  signIn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActionsEnum.INIT_SIGN_IN),
      mergeMap((action: any) =>
        this._authService.getUser(action.userId).pipe(
          map((response: any) =>
            changeAuthState({
              user: response,
              session: true,
              suspension: response.suspension.status,
              error: false,
              message: 'User loaded successfully.',
            }),
          ),
          catchError((error) =>
            of(
              changeAuthStateError({
                message: error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  singOut$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActionsEnum.INIT_SIGN_OUT),
      mergeMap((action: any) =>
        this._authService
          .signOutAccount({
            userId: action.userId,
            sesionId: action.sessionId,
          })
          .pipe(
            map((response: any) => logOutAccountAction(response)),
            catchError((error) =>
              of(
                changeAuthStateError({
                  message: error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );
}
