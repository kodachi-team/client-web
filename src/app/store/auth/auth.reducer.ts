import { createReducer, on, Action } from '@ngrx/store';
import {
  changeAuthState,
  initChangeAuthState,
  changeAuthStateError,
  logOutAccountAction,
  initSignOutAction,
} from './auth.action';

export interface authStateI {
  user: any;
  session: boolean;
  suspension: boolean;
  error: boolean;
  message: string;
  loading: boolean;
}

export const authInitState: authStateI = {
  user: null,
  session: false,
  suspension: false,
  error: false,
  message: null,
  loading: false,
};

const _authReducer = createReducer(
  authInitState,
  on(initChangeAuthState, (state) => ({
    ...state,
    loading: true,
  })),
  on(initSignOutAction, (state) => ({
    ...state,
    loading: true,
  })),
  on(
    changeAuthState,
    (state, { user, session, suspension, error, message }) => ({
      ...state,
      user: user,
      session: session,
      suspension: suspension,
      error: error,
      message: message,
      loading: false,
    }),
  ),
  on(changeAuthStateError, (state, { message }) => ({
    ...state,
    user: null,
    session: false,
    suspension: false,
    error: true,
    message,
    loading: false,
  })),
  on(logOutAccountAction, (state, { message }) => ({
    ...state,
    user: null,
    session: false,
    suspension: false,
    error: false,
    message,
    loading: false,
  })),
);

export function authReducer(state: authStateI, action: Action) {
  return _authReducer(state, action);
}
