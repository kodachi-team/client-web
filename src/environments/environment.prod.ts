export const environment = {
  production: true,
  api: 'https://kodachi-api.herokuapp.com/v1',
  title: '— Kodachi - The best anime tracker',
  subtitle: '— Kodachi',
  urlApp: 'https://kodachiapp.netlify.com',
};
